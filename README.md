## Name
The Occurence of Pi in the Mandelbrot Set

## Description
We upload our self-written programs to this repository. The programs serve the experimental part of our work for Regeneron ISEF 2025.

## Contact
If there are any problems or inquiries, you can contact us at 1410nedim@gmail.com.


## Project status
This project is currently in progress and more programs may be added over time.