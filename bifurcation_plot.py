import csv
import matplotlib.pyplot as plt

real = lambda list: [element.real for element in list]
imag = lambda list: [element.imag for element in list]

data = []
with open('./bifurcation_points.csv', mode='r', newline='', encoding='utf-8') as file:
    reader = csv.reader(file)
    for row in reader:
        data.append(complex(row[0]))

plt.scatter(real(data), imag(data), color='red')
plt.imshow(plt.imread("./imgs/proto_mandelbrot.png"), extent=[-2, 0.48, -1.24, 1.24])
plt.show()