import matplotlib.pyplot as plt
import numpy as np

z = lambda zeta, q: (-q*zeta)**(-1/q)
fixpoint_calc = lambda p, q, eps, i: abs(eps)**(q/p) * np.exp(np.pi*1j*(2*i + 1)*(q/p))
derivative_iteration = lambda p, q, z: 1 - (p/q)*z**((p/q)-1)
real_part = lambda list: [element.real for element in list]
imaginary_part = lambda list: [element.imag for element in list]

class Ecalle():
    def __init__(self, eps, p, q, start_orbit_amount, iteration_amount, positiv_axis_iter, orbit_main = []):
        self.eps = eps
        self.p = p
        self.q = q
        self.start_orbit_amount = start_orbit_amount
        self.iteration_amount = iteration_amount
        self.orbit_main = orbit_main
        self.positiv_axis_iter = positiv_axis_iter


    def plot_fixpoints(self):

        fixpoints_list = []
        attractiveness = []

        for i in range(100):
            # All fixpoints who's argument is in the principal branch arg(z) ∈ (-π, π]

            if ((2*i) + 1) *(self.q/self.p) > 1:
                break

            fixpoint = fixpoint_calc(self.p, self.q, self.eps, i)
            fixpoints_list.append(fixpoint)
            fixpoints_list.append(np.conjugate(fixpoint))
            attractiveness.append(abs(derivative_iteration(self.p, self.q, fixpoint)))
            attractiveness.append(abs(derivative_iteration(self.p, self.q, np.conjugate(fixpoint))))
            

        for i in range(len(attractiveness)):
            if attractiveness[i] > 1:
                plt.scatter(fixpoints_list[i].real, fixpoints_list[i].imag, color='red', zorder=4)
            elif attractiveness[i] < 1:
                plt.scatter(fixpoints_list[i].real, fixpoints_list[i].imag, color='blue', zorder=4)

        plt.scatter(0, 0, color='black', zorder=3)
        

    def place_and_iterate(self):
        
        orbit = []
       
        fix_a = fixpoint_calc(self.p, self.q, 0.001, 1)
        fix_b = fixpoint_calc(self.p, self.q, 0.001, -1)
        print(fix_a, fix_b)
        # Place starting orbits        
        for j in range(-self.start_orbit_amount, self.start_orbit_amount):
            #orbit.append(z(complex(-10, j*100), (self.p/self.q)-1))
            orbit.append(complex((fix_a.real - fix_b.real) * j / (2*self.start_orbit_amount), (fix_a.imag - fix_b.imag) * j / (2*self.start_orbit_amount)))
        
        orbit_attractive = orbit
        orbit_repelling = orbit

        orbit_real = fix_a.real
        index = 0 
        iter = 0
        while orbit_real < self.positiv_axis_iter and iter < 100000:
            try:
                orbit_repelling = list(map(lambda z: z + z**(self.p/self.q) + self.eps, orbit_repelling))
                self.orbit_main.append(orbit_repelling)
                orbit_real = orbit_real + orbit_real**(self.p/self.q) + self.eps
                index += 1
            except OverflowError:
                print('OverflowError')
                break
            iter += 1
        
        orbit_real = fix_a.real
        index = 0
        iter = 0
        while orbit_real > 0 and iter < 100000:
            try:
                orbit_attractive = list(map(lambda z: z - z**(self.p/self.q) - self.eps, orbit_attractive))
                self.orbit_main.append(orbit_attractive)
                orbit_real = orbit_real - orbit_real**(self.p/self.q) - self.eps
                index += 1
            except OverflowError:
                print('OverflowError')
                break
            iter += 1
        

    def connect_iteration(self, nthiteration):    
        print('Starting to connect iterations')
        
        for i in range(0, len(self.orbit_main), nthiteration):
            plt.plot(real_part(self.orbit_main[i]), imaginary_part(self.orbit_main[i]), color='black', zorder=2)

    def connect_orbit(self, nthiteration):
        print('Starting to connect orbits')
        for i in range(len(self.orbit_main[0])):
            temp = []
    
            if i % nthiteration == 0:
                for sublist in self.orbit_main:
                    temp.append(sublist[i])
            
                plt.plot(real_part(temp), imaginary_part(temp), zorder=2)
    
    def rotating_n_shit(self):
        pass
    
    def img_main(self):
        self.plot_fixpoints()

        pass


ecalle = Ecalle(
    eps = 0,
    p = 3,
    q = 1,
    start_orbit_amount = 100,
    iteration_amount = 100,
    positiv_axis_iter = 2,
)   
ecalle.plot_fixpoints()
ecalle.place_and_iterate()
ecalle.connect_iteration(10)

plt.gca().set_aspect('equal')
bound_calc = fixpoint_calc(3, 1, 0.0001, 0)

plt.savefig('ecallecylinder_rotation2.png', dpi=500)
plt.show()
