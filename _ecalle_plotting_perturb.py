import matplotlib.pyplot as plt
import numpy as np
import colorsys


z = lambda zeta, q: (-q*zeta)**(-1/q)
real_part = lambda list: [element.real for element in list]
imaginary_part = lambda list: [element.imag for element in list]

class Ecalle():
    def __init__(self, eps, p, q, start_orbit_amount, positiv_axis_iter, orbit_main_attracting = [], orbit_main_repelling = [], start_orbit_axis = 10):
        self.eps = eps
        self.p = p
        self.q = q
        self.start_orbit_amount = start_orbit_amount
        self.orbit_main_repelling = orbit_main_repelling
        self.orbit_main_attracting = orbit_main_attracting
        self.positiv_axis_iter = positiv_axis_iter
        self.start_orbit_axis = eps**(1/2)

    def place_and_iterate(self):
        orbit_repelling, orbit_attractive = [], []
        # Place starting orbits        
        for j in range(2*self.start_orbit_amount):
            try: 
                orbit_repelling.append(complex(0, self.start_orbit_axis*(j/self.start_orbit_amount) - self.start_orbit_axis))
            except ZeroDivisionError:
                print('ZeroDivisionError')
                pass

        for j in range(2*self.start_orbit_amount):
            try: 
                orbit_attractive.append(complex(0, self.start_orbit_axis*(j/self.start_orbit_amount) - self.start_orbit_axis))
            except ZeroDivisionError:
                print('ZeroDivisionError')
                pass
        
        
        print('Iterating Attractive') # RED
        iter = 0
        # iter < (1/2) * (np.pi/(self.eps**0.5))
        while iter < 1000:
            try:
                orbit_attractive = list(map(lambda z: z**(self.p/self.q) + z + self.eps, orbit_attractive))
                self.orbit_main_attracting.append(orbit_attractive)
                iter += 1
            except OverflowError:
                print('OverflowError')
                break
        
        print('Iterating Repelling') # BLUE
        iter = 0
        # iter < (1/2) * (np.pi/(self.eps**0.5))
        while min([abs(i) for i in orbit_repelling]).real < self.eps:
            try:
                orbit_repelling = list(map(lambda z: z**(self.p/self.q) - z + self.eps, orbit_repelling))
                self.orbit_main_repelling.append(orbit_repelling)
                iter += 1
            except OverflowError:
                print('OverflowError')
                break
        
    def plot_fixed_points(self):
        plt.scatter(0, self.eps**(1/2), color='green', zorder=4)
        plt.scatter(0, -self.eps**(1/2), color='green', zorder=4)

        # Set frame limits
        plt.gca().set_ylim(-2*self.eps**(1/2), 2*self.eps**(1/2))
        plt.gca().set_xlim(-3*self.eps**(1/2), 3*self.eps**(1/2))

    
    def generate_color_palette(self, num_colors, direction):
        base_hue = 120.0 / 360.0  # Green

        color_palette = []
        for i in range(num_colors):
            hue = (base_hue + direction*(i / num_colors) * 0.5) % 1.0
            rgb_color = colorsys.hsv_to_rgb(hue, 1.0, 1.0)
            hex_color = '#{:02x}{:02x}{:02x}'.format(int(rgb_color[0]*255), int(rgb_color[1]*255), int(rgb_color[2]*255))
            color_palette.append(hex_color)
        return color_palette


    def connect_iteration(self, nthiteration):    
        print('Starting to connect iterations')
        colors = self.generate_color_palette(len(self.orbit_main_attracting), 1)
        for i in range(0, len(self.orbit_main_attracting) - 1, nthiteration):
            plt.plot(real_part(self.orbit_main_attracting[i]), imaginary_part(self.orbit_main_attracting[i]), color=colors[i], zorder=1)

        colors = self.generate_color_palette(len(self.orbit_main_repelling), -1)
        for i in range(0, len(self.orbit_main_repelling), nthiteration):  
            plt.plot(real_part(self.orbit_main_repelling[i]), imaginary_part(self.orbit_main_repelling[i]), color=colors[i], zorder=1)
    
    def connect_orbit(self, nthiteration):
        print('Starting to connect orbits')
        for i in range(len(self.orbit_main[0])):
            temp = []
    
            if i % nthiteration == 0:
                for sublist in self.orbit_main:
                    temp.append(sublist[i])
            
                plt.plot(real_part(temp), imaginary_part(temp), zorder=2)

print('Initializing')
ecalle = Ecalle(
    eps = 0.001,
    p = 2,
    q = 1,
    start_orbit_amount = 10000,
    positiv_axis_iter = 2,
)
ecalle.place_and_iterate()
ecalle.plot_fixed_points()
ecalle.connect_iteration(50)


plt.gca().set_aspect('equal')
plt.savefig('ecallecylinder_pertubiert_e-6_nth_50.png', dpi=500)
plt.show()