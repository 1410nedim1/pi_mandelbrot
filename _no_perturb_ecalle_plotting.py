import matplotlib.pyplot as plt
import numpy as np

real = lambda list: [element.real for element in list]
imag = lambda list: [element.imag for element in list]

class Ecalle():
    def __init__(self, q, starting_orbits, zeta_real, zeta_imag, iter_amount_to_origin):
        self.q = q
        self.starting_orbits = starting_orbits
        self.zeta_real = zeta_real
        self.zeta_imag = zeta_imag
        self.iter_amount_to_origin = iter_amount_to_origin
        self.orbit_list = []

    # Repelling
    def r_f(self, z):
       try:
           return z**(self.q+1) + z
       except OverflowError:
           print('OverflowError')
           return 0
       except FloatingPointError:
           print('FloatingPointError')
           return 0
    def r_df(self, z):
       return (self.q+1)*z**self.q + 1
   
   # Attractive
    def a_f(self, z):
       try:
           return z - z**(self.q+1)
       except OverflowError: 
           print('OverflowError')
           return 0
       except FloatingPointError:
           print('FloatingPointError')
           return 0
    def a_df(self, z):
        return -(self.q+1)*z**self.q + 1
    
    def zeta_to_z(self, zeta):
        return (-self.q * zeta)**(-1/self.q)
    
    def rotate_ecalle(self, z, times):
        return z*np.exp((2*np.pi*1j*times)/(2*self.q))

    def place_and_iterate(self):
        
        orbit = []
        for i in range(self.starting_orbits):
            orbit.append(complex(self.zeta_real, 0.000000001*((2*self.zeta_imag*(i/self.starting_orbits) - self.zeta_imag)**5)))

        orbit = list(map(self.zeta_to_z, orbit))
        orbit = [0] + orbit + [0]

        orbit_temp = orbit.copy()
        
        for i in range(self.iter_amount_to_origin):
            orbit = list(map(self.a_f, orbit))
            self.orbit_list.append(list(map(self.a_f, orbit)))
            if i%1000==0:
                print(f'Iter to origin... {i/self.iter_amount_to_origin}')

        '''
        orbit = orbit_temp
        iteration = 0
        while abs(orbit[int(len(orbit)/2)]) < 2:
            orbit = list(map(self.r_f, orbit))
            self.orbit_list.append(list(map(self.r_f, orbit)))

            if iteration%100==0:
                print(f'Iter to infinity... {abs(orbit[int(len(orbit)/2)])}')
            iteration += 1
        '''

    def generate_colors(self, length, red_flag):
        colors = []
        if red_flag:
            for i in range(length):
                if length == 1:
                    ratio = 0.0
                else:
                    # Non-linear interpolation using square root function
                    ratio = i / (length - 1)
                    ratio = ratio ** (0.125/1.5)
                # Interpolate RGB values from white to blue
                r = 255
                g = int(255 * (1 - ratio))
                b = int(255 * (1 - ratio))
                hex_color = "#{:02x}{:02x}{:02x}".format(r, g, b)
                colors.append(hex_color)
        else:
            for i in range(length):
                if length == 1:
                    ratio = 0.0
                else:
                    # Non-linear interpolation using square root function
                    ratio = i / (length - 1)
                    ratio = ratio ** 0.5
                # Interpolate RGB values from white to blue
                r = int(255 * (1 - ratio))
                g = int(255 * (1 - ratio))
                b = 255
                hex_color = "#{:02x}{:02x}{:02x}".format(r, g, b)
                colors.append(hex_color)
        return colors

        
    def plot_petals(self, nthiteration):       
            print('plotting...')
            
            for theta in range(0, 2*self.q):
                rotated_petal = []
                print(theta/(2*self.q))
                for j in range(0, len(self.orbit_list), nthiteration):
                    rotated_petal.append(list(map(lambda z: self.rotate_ecalle(z, times=theta), self.orbit_list[j])))

                    for i, sublist in enumerate(rotated_petal):
                        if theta%2==0:
                            color = self.generate_colors(len(rotated_petal), True)
                        else:
                            color = self.generate_colors(len(rotated_petal), False)
                            color = color[::-1]
                            color[0] = 'white'

                        plt.plot(real(sublist), imag(sublist), color=color[i]) # alpha=(i)/self.iter_amount_to_origin

    def q_1_plot(self):
            plt.text(-0.00075, -0.0005, r'$P_{1, -}$')
            plt.text(0.00075, -0.0005, r'$P_{1, +}$')

    def q_2_plot(self):
        plt.text(0.01, 0.02, r'$P_{1, -}$')
        plt.text(0.02, -0.01, r'$P_{1, +}$')
        plt.text(-0.01, -0.02, r'$P_{2, -}$')
        plt.text(-0.02, 0.01, r'$P_{2, +}$')
    
    def plot(self):
        plt.gca().set_aspect('equal', adjustable='box')
        #plt.axhline(0, color='black', linewidth=.5)
        #plt.axvline(0, color='black', linewidth=.5)
        plt.scatter(0, 0, color='green', zorder=4) # fixed point
        plt.xlim(-0.001, 0.001)
        plt.ylim(-0.001, 0.001)
        plt.axis('off')

        
        plt.savefig(f'E:\VSCode\mandel-python\\petals_colored_q_{self.q}.png', bbox_inches='tight', pad_inches=0, dpi=500)
        plt.show()

ecalle = Ecalle(q = 5,
                starting_orbits = 200,
                zeta_real = -200,
                zeta_imag = -1000,
                iter_amount_to_origin = 100000,
                )
                
ecalle.place_and_iterate()
ecalle.plot_petals(nthiteration = 1000,)
#ecalle.q_1_plot()
ecalle.plot()