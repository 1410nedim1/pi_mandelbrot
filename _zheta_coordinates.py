import matplotlib.pyplot as plt
import numpy as np

z = lambda zeta, q: (-q*zeta)**(-1/q)
real = lambda list: [element.real for element in list]
imag = lambda list: [element.imag for element in list]

q = 4
ma = 50

orbit = []
for i in range(ma):
    orbit.append(np.exp(1j*np.pi*(i/ma)*(1/q) + np.pi*1j/2*q))

plt.scatter(real(orbit), imag(orbit), color='r', zorder=2)


orbit_iter = list(map(lambda zeta: z(zeta, q), orbit))


for i in range(len(orbit)):
    plt.plot([real(orbit)[i], real(orbit_iter)[i]], [imag(orbit)[i], imag(orbit_iter)[i]], color='b')
plt.scatter(real(orbit_iter), imag(orbit_iter), color='orange', zorder=3)

plt.gca().set_aspect('equal', adjustable='box')
plt.show()