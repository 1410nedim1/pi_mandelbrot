import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import newton # Used for fixedpoint calc

real = lambda list: [element.real for element in list]
imag = lambda list: [element.imag for element in list]

fixpoint_calc = lambda p, q, eps, i: abs(eps)**(q/p) * np.exp(np.pi*1j*(2*i + 1)*(q/p))
derivative_iteration = lambda p, q, z: 1 - (p/q)*z**((p/q)-1)

np.seterr('raise')
class Field:
    def __init__(self, eps, p, q, orbit_amount, neighbourhood):
        self.eps = eps
        self.p = p
        self.q = q
        self.orbit_amount = orbit_amount
        self.neighbourhood = neighbourhood
        self.main_orbit_list_attractive = []
        self.main_orbit_list_repelling = []
        self.iterations = 0
    
    '''
    # This is for the analysis of the conjugated map for P_3
    def r_f(self, z):
        
        fpoint = -1/4 * np.sqrt(-6j*np.sqrt(3) + 6) + 1/2
        z = z + fpoint
        #plt.scatter(fpoint.real, fpoint.imag)
        p = lambda z: (43046721/33554432*self.eps**16*(-1j*np.sqrt(3) - 1) + 4782969/1048576*self.eps**15*(np.sqrt(3) - 1j) + 531441/524288*self.eps**14*(-2*1j*np.sqrt(3) + 7) + 413343/131072*self.eps**13*(7*np.sqrt(3) + 2*1j) + 6561/524288*self.eps**12*(1043*1j*np.sqrt(3) + 935) + 729/65536*self.eps**11*(2695*np.sqrt(3) + 3533*1j) + 81/32768*self.eps**10*(9652*1j*np.sqrt(3) - 4991) + 9/8192*self.eps**9*(23647*np.sqrt(3) + 41342*1j) - 1/65536*self.eps**8*(-1094281*1j*np.sqrt(3) + 1404911) + 1/4096*self.eps**7*(91807*np.sqrt(3) + 135513*1j) + 3/2048*self.eps**6*(5098*1j*np.sqrt(3) - 8477) + 3/512*self.eps**5*(2405*np.sqrt(3) + 2934*1j) - 1/2048*self.eps**4*(-3343*1j*np.sqrt(3) + 6133) + 1/256*self.eps**3*(1289*np.sqrt(3) + 1419*1j) - 1/128*self.eps**2*(-32*1j*np.sqrt(3) + 49) + 1/32*self.eps*(17*np.sqrt(3) + 18*1j) + 39/512*1j*np.sqrt(3) - 41/512) + (-531441/65536*self.eps**12 - 177147/8192*1j*self.eps**11 + 19683/16384*self.eps**10*(-9*1j*np.sqrt(3) - 7) + 3645/4096*self.eps**9*(27*np.sqrt(3) - 65*1j) + 243/4096*self.eps**8*(-153*1j*np.sqrt(3) + 298) + 27/256*self.eps**7*(423*np.sqrt(3) - 440*1j) + 1/512*self.eps**6*(621*1j*np.sqrt(3) + 6755) + 3/128*self.eps**5*(1415*np.sqrt(3) - 1101*1j) + 1/256*self.eps**4*(-358*1j*np.sqrt(3) - 173) + 1/32*self.eps**3*(466*np.sqrt(3) - 393*1j) + 1/64*self.eps**2*(-175*1j*np.sqrt(3) - 193) + 1/16*self.eps*(47*np.sqrt(3) - 45*1j) - 15/16*1j*np.sqrt(3) - 1)*z**2 + (19683/4096*self.eps**8*(1j*np.sqrt(3) - 1) - 2187/256*self.eps**7*(np.sqrt(3) + 1j) + 729/256*self.eps**6*(1j*np.sqrt(3) - 7) - 27/64*self.eps**5*(23*np.sqrt(3) + 77*1j) + 3/128*self.eps**4*(-73*1j*np.sqrt(3) - 683) - 3/16*self.eps**3*(11*np.sqrt(3) + 135*1j) + 1/16*self.eps**2*(-1j*np.sqrt(3) - 145) - 1/4*self.eps*(np.sqrt(3) + 27*1j) + 3/16*1j*np.sqrt(3) - 43/16)*z**4 + (81/32*self.eps**4*(1j*np.sqrt(3) + 1) - 9/4*self.eps**3*(np.sqrt(3) - 1j) + 1/2*self.eps**2*(8*1j*np.sqrt(3) - 1) - 2*np.sqrt(3)*self.eps + 3/2*1j*np.sqrt(3) - 1/2)*z**6 + z**8
        try:
            return p(z) - fpoint
        except OverflowError:
            print('OverflowError')
            return 0
        except FloatingPointError:
            print('FloatingPointError')
            return 0
    def r_df(self, z):
        fpoint = -1/4 * np.sqrt(-6j*np.sqrt(3) + 6) + 1/2
        z = z + fpoint
        dp = lambda z: 8*z**7 + 3/16*(81*self.eps**4*(1j*np.sqrt(3) + 1) - 72*self.eps**3*(np.sqrt(3) - 1j) + 16*self.eps**2*(8*1j*np.sqrt(3) - 1) - 64*np.sqrt(3)*self.eps + 48*1j*np.sqrt(3) - 16)*z**5 + 1/1024*(19683*self.eps**8*(1j*np.sqrt(3) - 1) - 34992*self.eps**7*(np.sqrt(3) + 1j) + 11664*self.eps**6*(1j*np.sqrt(3) - 7) - 1728*self.eps**5*(23*np.sqrt(3) + 77*1j) + 96*self.eps**4*(-73*1j*np.sqrt(3) - 683) - 768*self.eps**3*(11*np.sqrt(3) + 135*1j) + 256*self.eps**2*(-1j*np.sqrt(3) - 145) - 1024*self.eps*(np.sqrt(3) + 27*1j) + 768*1j*np.sqrt(3) - 11008)*z**3 - 1/32768*(531441*self.eps**12 + 1417176*1j*self.eps**11 - 78732*self.eps**10*(-9*1j*np.sqrt(3) - 7) - 58320*self.eps**9*(27*np.sqrt(3) - 65*1j) - 3888*self.eps**8*(-153*1j*np.sqrt(3) + 298) - 6912*self.eps**7*(423*np.sqrt(3) - 440*1j) - 128*self.eps**6*(621*1j*np.sqrt(3) + 6755) - 1536*self.eps**5*(1415*np.sqrt(3) - 1101*1j) - 256*self.eps**4*(-358*1j*np.sqrt(3) - 173) - 2048*self.eps**3*(466*np.sqrt(3) - 393*1j) - 1024*self.eps**2*(-175*1j*np.sqrt(3) - 193) - 4096*self.eps*(47*np.sqrt(3) - 45*1j) + 61440*1j*np.sqrt(3) + 65536)*z
        return dp(z)
    def a_f(self, z):
        p = lambda z: -(43046721/33554432*self.eps**16*(-1j*np.sqrt(3) - 1) + 4782969/1048576*self.eps**15*(np.sqrt(3) - 1j) + 531441/524288*self.eps**14*(-2*1j*np.sqrt(3) + 7) + 413343/131072*self.eps**13*(7*np.sqrt(3) + 2*1j) + 6561/524288*self.eps**12*(1043*1j*np.sqrt(3) + 935) + 729/65536*self.eps**11*(2695*np.sqrt(3) + 3533*1j) + 81/32768*self.eps**10*(9652*1j*np.sqrt(3) - 4991) + 9/8192*self.eps**9*(23647*np.sqrt(3) + 41342*1j) - 1/65536*self.eps**8*(-1094281*1j*np.sqrt(3) + 1404911) + 1/4096*self.eps**7*(91807*np.sqrt(3) + 135513*1j) + 3/2048*self.eps**6*(5098*1j*np.sqrt(3) - 8477) + 3/512*self.eps**5*(2405*np.sqrt(3) + 2934*1j) - 1/2048*self.eps**4*(-3343*1j*np.sqrt(3) + 6133) + 1/256*self.eps**3*(1289*np.sqrt(3) + 1419*1j) - 1/128*self.eps**2*(-32*1j*np.sqrt(3) + 49) + 1/32*self.eps*(17*np.sqrt(3) + 18*1j) + 39/512*1j*np.sqrt(3) - 41/512) + (-531441/65536*self.eps**12 - 177147/8192*1j*self.eps**11 + 19683/16384*self.eps**10*(-9*1j*np.sqrt(3) - 7) + 3645/4096*self.eps**9*(27*np.sqrt(3) - 65*1j) + 243/4096*self.eps**8*(-153*1j*np.sqrt(3) + 298) + 27/256*self.eps**7*(423*np.sqrt(3) - 440*1j) + 1/512*self.eps**6*(621*1j*np.sqrt(3) + 6755) + 3/128*self.eps**5*(1415*np.sqrt(3) - 1101*1j) + 1/256*self.eps**4*(-358*1j*np.sqrt(3) - 173) + 1/32*self.eps**3*(466*np.sqrt(3) - 393*1j) + 1/64*self.eps**2*(-175*1j*np.sqrt(3) - 193) + 1/16*self.eps*(47*np.sqrt(3) - 45*1j) - 15/16*1j*np.sqrt(3) - 1)*z**2 - (19683/4096*self.eps**8*(1j*np.sqrt(3) - 1) - 2187/256*self.eps**7*(np.sqrt(3) + 1j) + 729/256*self.eps**6*(1j*np.sqrt(3) - 7) - 27/64*self.eps**5*(23*np.sqrt(3) + 77*1j) + 3/128*self.eps**4*(-73*1j*np.sqrt(3) - 683) - 3/16*self.eps**3*(11*np.sqrt(3) + 135*1j) + 1/16*self.eps**2*(-1j*np.sqrt(3) - 145) - 1/4*self.eps*(np.sqrt(3) + 27*1j) + 3/16*1j*np.sqrt(3) - 43/16)*z**4 + (81/32*self.eps**4*(1j*np.sqrt(3) + 1) - 9/4*self.eps**3*(np.sqrt(3) - 1j) + 1/2*self.eps**2*(8*1j*np.sqrt(3) - 1) - 2*np.sqrt(3)*self.eps + 3/2*1j*np.sqrt(3) - 1/2)*z**6 + z**8
        fpoint = -1/4 * np.sqrt(-6j*np.sqrt(3) + 6) + 1/2
        z = z + fpoint
        return p(z) - fpoint
    '''
    '''
    # This is for the analysis of the conjugated map for c=-5/4
    def r_f(self, z):
        p = lambda z: z**2 -5/4 -self.eps**2 + 1j*self.eps
        try:
            return p(p(p(p(z + (- 1/2 + np.sqrt(1/2)))))) - (- 1/2 + np.sqrt(1/2))
        except OverflowError:
            print('OverflowError')
            return 0
        except FloatingPointError:
            print('FloatingPointError')
            return 0
    def r_df(self, z):
        p = lambda z: z**2 -5/4 -self.eps**2 + 1j*self.eps
        dp = lambda z: 2*z
        return dp(p(p(p(z - 1/2 + np.sqrt(1/2)))))*dp(p(p(z - 1/2 + np.sqrt(1/2))))*dp(p(z - 1/2 + np.sqrt(1/2)))*dp(z - 1/2 + np.sqrt(1/2))
    def a_f(self, z):
        p = lambda z: z**2 -5/4 -self.eps**2 + 1j*self.eps
        p_1 = lambda y: np.sqrt(y+5/4+self.eps**2 - 1j*self.eps)
        try:
            print(z, p_1(p_1(p_1(p_1(z + (- 1/2 + np.sqrt(1/2)))))) - (- 1/2 + np.sqrt(1/2)))
            return p_1(z + (- 1/2 + np.sqrt(1/2))) - (- 1/2 + np.sqrt(1/2))
        except OverflowError: 
            print('OverflowError')
            return 0
        except FloatingPointError:
            print('FloatingPointError')
            return 0
    '''
    
    # This is for the analysis of the conjugated map for c=-3/4
    def r_f(self, z):
        try:
            return  -1/4*np.sqrt(2)*(1j*z**4 - 2*np.sqrt(2)*z**3 + 4*self.eps*z**2 - 4*1j*self.eps**2 + 2*(2*1j*np.sqrt(2)*self.eps - np.sqrt(2))*z) 
        except OverflowError:
            print('OverflowError')
            return 0
        except FloatingPointError:
            print('FloatingPointError')
            return 0
    def r_df(self, z):
        return  -1j*np.sqrt(2)*z**3 - 2*np.sqrt(2)*self.eps*z + 3*z**2 - 2*1j*self.eps + 1
    # Attractive
    def a_f(self, z):
        try:
            return   + z - z**3 + np.sqrt(2)*self.eps*z**2 + 2*1j*self.eps*z - np.sqrt(2)*1j*self.eps**2 # (z**4)*-1j*np.sqrt(2)/4
        except OverflowError: 
            print('OverflowError')
            return 0
        except FloatingPointError:
            print('FloatingPointError')
            return 0
    def a_df(self, z):
        return (z**3)*-4j*np.sqrt(2)/4 + 1 - 3*z**2 - 2*np.sqrt(2)*self.eps*z - 2*1j*self.eps
    
    '''
    # Repelling
    def r_f(self, z):
       try:
           #print(z**(self.p/self.q) + z + self.eps)
           return z**(self.p/self.q) + z + self.eps
       except OverflowError:
           print('OverflowError')
           return None
       except FloatingPointError:
           print('FloatingPointError')
           return None
    def r_df(self, z):
       return (self.p/self.q)*z**(self.p/self.q - 1) + 1
   # Attractive
    def a_f(self, z):
       try:
           #print(z - z**(self.p/self.q) - self.eps)
           return z - z**(self.p/self.q) - self.eps
       except OverflowError: 
           print('OverflowError')
           return 0
       except FloatingPointError:
           print('FloatingPointError')
           return 0
    def a_df(self, z):
        return (self.p/self.q)*z**(self.p/self.q - 1) - 1
    '''

    # Angle coloring depending on the derivative of the position of the orbit
    def phase_color(self, angle):
        # Normalize the phase angle to the range [0, 2*pi]
        angle = angle % (2 * np.pi)
        
        red = np.clip(255 * (np.cos(angle) + 1) / 2, 0, 255)
        green = np.clip(255 * (np.cos(angle + 2 * np.pi / 3) + 1) / 2, 0, 255)
        blue = np.clip(255 * (np.cos(angle + 4 * np.pi / 3) + 1) / 2, 0, 255)
        
        return '#{0:02x}{1:02x}{2:02x}'.format(int(red), int(green), int(blue))

    # Colors the vector depending on wether the orbit goes towards or repells from zero
    def petal_color(self, first, second):
        if abs(first) - abs(second) < 0:
            # Orbit going outwords --> Repelling
            return 'red'
        elif abs(first) - abs(second) > 0:
            # Orbit going inward --> Attractive
            return 'blue'
    
    # Standard fixedpoint function from coordinate_projection. It works.
    def plot_fixpoints(self):

        fixpoints_list = []
        attractiveness = []

        for i in range(100):
            # All fixpoints who's argument is in the principal branch arg(z) ∈ (-π, π]

            if ((2*i) + 1) *(self.q/self.p) > 1:
                break

            fixpoint = fixpoint_calc(self.p, self.q, self.eps, i)
            fixpoints_list.append(fixpoint)
            fixpoints_list.append(np.conjugate(fixpoint))
            attractiveness.append(abs(derivative_iteration(self.p, self.q, fixpoint)))
            attractiveness.append(abs(derivative_iteration(self.p, self.q, np.conjugate(fixpoint))))
            

        for i in range(len(attractiveness)):
            if attractiveness[i] > 1: # Red is repulsive    
                plt.scatter(fixpoints_list[i].real, fixpoints_list[i].imag, color='red', zorder=4)
            elif attractiveness[i] < 1: # Blue is attractive
                plt.scatter(fixpoints_list[i].real, fixpoints_list[i].imag, color='blue', zorder=4)

        # Plot zero
        plt.scatter(0, 0, color='orange', zorder=3)

        # Plot critical points
        critical_point_calc = lambda k, q: np.exp((1j*np.pi/q) + (2*np.pi*1j*k/q)) * (q+1)**(-1/q)
        for k in range(int(self.p)):
            critical_point = critical_point_calc(k, self.p)
            plt.scatter(critical_point.real, critical_point.imag, color='green', zorder=5)

    # Create complex grid in neighborhood 0
    # Orbits are placed on a grid in a neighbourhood, preferably close to zero
    def complex_grid(self):
        real_range = np.linspace(-self.neighbourhood, self.neighbourhood, self.orbit_amount)
        imag_range = np.linspace(-self.neighbourhood, self.neighbourhood, self.orbit_amount)
        real_part, imag_part = np.meshgrid(real_range, imag_range)
        complex_grid = real_part + 1j * imag_part
        unchained = [num for sublist in complex_grid for num in sublist]
        return unchained

    # Orbits are placed on a complex grid in the given neigbourhood and iterated over.
    def place_and_iter(self, vector_field=False, iterate_attr=True):
        c_temp = self.complex_grid()

        if vector_field:
            complex_grid_repelling_prime = list(map(self.r_f, c_temp))
        
            if iterate_attr:
                complex_grid_attractive_prime = list(map(self.a_f, c_temp))
                return [complex_grid_attractive_prime + c_temp, c_temp + complex_grid_repelling_prime]
            else:
                return [c_temp, complex_grid_repelling_prime]
        else:
            return c_temp

    # plots a vector field
    def vector_field(self):
        
        orbit_main = []
        orbit_main = self.place_and_iter(True, False)

        for i in range(len(orbit_main) - 1):
            first_iter = orbit_main[i]
            second_iter = orbit_main[i + 1]

            for a, b in zip(first_iter, second_iter):
                plt.quiver(a.real, a.imag, 
                        b.real - a.real, b.imag - a.imag,
                        color='black', width=0.004,
                        alpha = 0.35
                        )

    def vector_field_irrational(self):
        
        orbit_main = []
        orbit_main = self.place_and_iter(True, False)
        
        x = np.linspace(0, -2, 10)
        y = x * (0.5/0.372)
        #plt.fill_between(x, y, alpha=0.35, color='red')
        #plt.fill_between(x, -y, alpha=0.35, color='red')
        
        for i in range(len(orbit_main) - 1):
            first_iter = orbit_main[i]
            second_iter = orbit_main[i + 1]
            #print(first_iter)
            for a, b in zip(first_iter, second_iter):

                #if abs(np.angle(a)) <  np.floor(2*(self.p-1))*np.pi/(2*(self.p-1)): 
                    plt.quiver(a.real, a.imag, 
                            b.real - a.real, b.imag - a.imag,
                            color='black', width=0.004,
                            alpha = 0.8
                            )
                    
        x = np.linspace(0, 2, 10)
        angle_list = []
        for i in range(-int(np.ceil(self.p-1)) + 1, int(np.ceil(self.p-1))):
            angle = 10*self.neighbourhood*np.exp(1j*np.pi*(i/(self.p-1)))
            angle_list.append(angle)
        angle_list_mean = [(angle_list[i] + angle_list[i + 1]) / 2 for i in range(len(angle_list) - 1)]

        # repulsive i guess        
        for i in range(0, len(angle_list_mean) - 1, 2):
            z0 = angle_list_mean[i]
            z1 = angle_list_mean[i + 1]

            plt.fill(real([0, z0, z1]), imag([0, z0, z1]), alpha=0.2, color='blue')

        # attractive ;)
        for i in range(1, len(angle_list_mean) - 1, 2):
            z0 = angle_list_mean[i]
            z1 = angle_list_mean[i + 1]

            plt.fill(real([0, z0, z1]), imag([0, z0, z1]), alpha=0.2, color='green')


        non_defined = [0, angle_list_mean[-1], angle_list_mean[0], 0]
        plt.fill(real(non_defined), imag(non_defined), alpha=0.2, color='red')
        plt.plot([-self.neighbourhood, 0], [0, 0], color='red', linewidth=3)
    
    def orbit(self, z):
        MAX_ITER = 100000
        orbit_iterations = []
        index = 0
        z_prime = 0
        plt.scatter(z.real, z.imag, color='blue', zorder=3)
        while index < MAX_ITER and abs(z) < 2:
                orbit_iterations.append(z)

                z = self.r_f(z)
                
                if abs(z - z_prime) < 1e-15:
                    print('orbit converged')
                    break
                z_prime = z
                index += 1
        
        orbit_iterations.append(z)    
        plt.plot(real(orbit_iterations), imag(orbit_iterations), color='orange', linewidth=4)
        self.iterations = index
        print(f'N(ε) = {index}')

    def newton_approximate_roots(self, d):
        fixedpoint_calc_map = lambda z: self.r_f(z) - z
        dfixedpoint_calc_map = lambda z:  self.r_df(z) - 1

        for i in range(4*d):
            x_0 = 1*np.exp(2j*np.pi*(i/(4*d)))
            #plt.scatter(x_0.real, x_0.imag)
            try:
                f_0 = newton(func=fixedpoint_calc_map, fprime=dfixedpoint_calc_map, x0=x_0, tol=1e-10, maxiter=10000)
                plt.scatter(f_0.real, f_0.imag, color='red')
            except FloatingPointError:
                print(f'newton was not able to find: {i}/{4*d}')
        print('roots plotted')

    def p_3_analysis(self, z=-(-1/4 * np.sqrt(-6j*np.sqrt(3) + 6) + 1/2)):

        fixedpoint_calc_map = lambda z: self.r_f(z) - z
        dfixedpoint_calc_map = lambda z:  self.r_df(z) - 1

        f1 = newton(func=fixedpoint_calc_map, fprime=dfixedpoint_calc_map, x0=0, tol=1e-10, maxiter=1000000)
        f2 = newton(func=fixedpoint_calc_map, fprime=dfixedpoint_calc_map, x0=0.173-0.0984j, tol=1e-10, maxiter=1000000)
        #f2 = 0.173-0.0984j

        print(np.angle(self.r_df(f1)), np.angle(self.r_df(f2)))

        self.orbit(z)
        #plt.scatter(f1.real, f1.imag, color='orange', zorder=5)
        #plt.scatter(f2.real, f2.imag, color='orange', zorder=5)
 
    def third_pi_point(self, z=-1/2 * np.sqrt(2) + 1/2):
        f_list = []
        error_flag = True

        fixedpoint_calc_map = lambda z: self.r_f(z) - z
        dfixedpoint_calc_map = lambda z:  self.r_df(z) - 1
        
        try:
            f1 = newton(func=fixedpoint_calc_map, fprime=dfixedpoint_calc_map, x0=-0.2+0.2j, tol=1e-10, maxiter=1000000)
            f2 = newton(func=fixedpoint_calc_map, fprime=dfixedpoint_calc_map, x0=0-0.05, tol=1e-10, maxiter=1000000)
            f3 = newton(func=fixedpoint_calc_map, fprime=dfixedpoint_calc_map, x0=0.2-0.2j, tol=1e-10, maxiter=1000000)
            
            plt.scatter(f1.real, f1.imag)
            plt.scatter(f2.real, f2.imag)
            plt.scatter(f3.real, f3.imag)
        except RuntimeError:
            error_flag = False
            print('RuntimeError, Could not plot fixedpoints!')
        print(f1, f2, f3)


        ###########################################################
        step_calc_f1, step_calc_f2, step_calc_f3 = 0, 0, 0
        orbit_tracking = []
        i = 0 
        
        print(np.angle(self.r_df(f1)), np.angle(self.r_df(f2)), np.angle(self.r_df(f3)))
        
        while abs(z) < 2 and i < 40000: 
            orbit_tracking.append(z)
            z = self.r_f(z)
            i += 1

            if error_flag:
                step_calc_f1 += np.angle(self.r_df(f1))
                step_calc_f2 += np.angle(self.r_df(f2))
                step_calc_f3 += np.angle(self.r_df(f3))

            if i % 10000 == 0:
                print(i)
        orbit_tracking.append(z)
        plt.plot(real(orbit_tracking), imag(orbit_tracking), linewidth=2, color='orange') 
        print(f'Iterations: {i}; Factor Pi: {4*i*self.eps/np.pi}')
        #print(f'Iteration Amount: {i}, \nAngle Summed f1: {step_calc_f1}, \nAngle Summed f2: {step_calc_f2},\nAngle Summed f3: {step_calc_f3}')      

        ###########################################################

    def second_pi_point(self, z=-1j*np.sqrt(2)/2):
        step_calc_f1, step_calc_f2, step_calc_f3 = 0, 0, 0 
        orbit_tracking = []

        fixedpoint_calc_map = lambda z:  self.r_f(z) - z
        dfixedpoint_calc_map = lambda z: self.r_df(z) - 1
        f1 = newton(fixedpoint_calc_map, fprime=dfixedpoint_calc_map, x0=0.3+0.3j, tol=1e-14, maxiter=1000)
        f2 = newton(fixedpoint_calc_map, fprime=dfixedpoint_calc_map, x0=-0.3-0.3j, tol=1e-14, maxiter=1000)
        f3 = newton(fixedpoint_calc_map, fprime=dfixedpoint_calc_map, x0=0, tol=1e-14, maxiter=1000)

        #plt.scatter(f1.real, f1.imag, color='red', zorder=3)
        #plt.scatter(f2.real, f2.imag, color='green', zorder=3)
        #plt.scatter(f3.real, f3.imag, color='blue', zorder=3)

        plt.scatter(f1.real, f1.imag, color='green', zorder=99)
        plt.scatter(f2.real, f2.imag, color='green', zorder=99)
        plt.scatter(f3.real, f3.imag, color='green', zorder=99)

        ############## THIS IS TEMPORARY. DELETE OR REPLACE ##############        
        
        '''
        # Place starting orbits
        orbit = []
        for j in range(1000):
            orbit.append(f3 + (f2 - f3) * (j / 1000))
            #orbit.append(f2 + (f1 - f2) * (j / 1000))
            #orbit.append(-1j + (-0.1j - 1j) * (j / 1000))

        iter_amount = int(314/4)
        
        for i in range(iter_amount):
            for j, element in enumerate(orbit):
                #print('now')
                element_prime = self.r_f(element)

                if element_prime == None or abs(element_prime) > 2:
                    del orbit[j]
                else:
                    orbit[j] = element_prime
        plt.scatter(real(orbit), imag(orbit), color='red', linewidth=4)
        '''            
        
        '''
        # This is to find the funnels for the correct start orbits
        for i in orbit:
            orbit_list = []
            for j in range(350):
                i = self.r_f(i)
                orbit_list.append(i)
                if abs(i) >= 2:
                    plt.scatter(real(orbit_list), imag(orbit_list), color='red')
                    break

        for i in orbit:
            orbit_list = []
            for j in range(350):
                i = self.a_f(i)
                orbit_list.append(i)
                if abs(i) >= 2:
                    plt.scatter(real(orbit_list), imag(orbit_list), color='green')
                    break
        '''
        
        # This is to make nice pictures for the ecalle cyllinders
        iter_amount = int(314/4)
        orbit = []

        # Place starting orbits        
        for j in range(1000):
            orbit.append(f3 + (f1 - f3) * (j / 1000))

        orbit_temp = orbit.copy()
        for i in range(iter_amount + 1):
            for j, element in enumerate(orbit):
                #print('now')
                element_prime = self.r_f(element)

                if element_prime == None or abs(element_prime) > 2:
                    del orbit[j]
                else:
                    orbit[j] = element_prime
            if i % 10 == 0:
                plt.plot(real(orbit), imag(orbit), color='red', linewidth=2)
        plt.plot(real(orbit), imag(orbit), color='red', linewidth=4)


        orbit = orbit_temp
        for i in range(iter_amount-0):
            for j, element in enumerate(orbit):
                #print('now')
                element_prime = self.a_f(element)

                if element_prime == None or abs(element_prime) > 2:
                    del orbit[j]
                else:
                    orbit[j] = element_prime
            if i % 10 == 0:
                plt.plot(real(orbit), imag(orbit), color='blue', linewidth=2)
        plt.plot(real(orbit), imag(orbit), color='blue', linewidth=4)

        orbit = []
        for j in range(1000):
            orbit.append(f2 + (f3 - f2) * (j / 1000))

        orbit_temp = orbit.copy()
        for i in range(iter_amount-1):
            for j, element in enumerate(orbit):
                #print('now')
                element_prime = self.r_f(element)

                if element_prime == None or abs(element_prime) > 2:
                    del orbit[j]
                else:
                    orbit[j] = element_prime
            if i % 10 == 0:
                plt.plot(real(orbit), imag(orbit), color='red', linewidth=2)
        plt.plot(real(orbit), imag(orbit), color='red', linewidth=4)

        orbit = orbit_temp
        for i in range(iter_amount + 1):
            for j, element in enumerate(orbit):
                #print('now')
                element_prime = self.a_f(element)

                if element_prime == None or abs(element_prime) > 2:
                    del orbit[j]
                else:
                    orbit[j] = element_prime
            if i % 10 == 0:
                plt.plot(real(orbit), imag(orbit), color='blue', linewidth=2)
        plt.plot(real(orbit), imag(orbit), color='blue', linewidth=4)
        
        ##################################################################

        print(np.angle(self.r_df(f1)), np.angle(self.r_df(f2)), np.angle(self.r_df(f3)))

        plt.scatter(z.real, z.imag, color='black', zorder=4)

        i = 0
        while abs(z) < 2 and i < 40000:
            
            orbit_tracking.append(z)
            z = self.r_f(z)
            #plt.scatter(z.real, z.imag, color='orange')
            step_calc_f1 += np.angle(self.r_df(f1))
            step_calc_f2 += np.angle(self.r_df(f2))
            step_calc_f3 += np.angle(self.r_df(f3))
            #print(step_calc)
            i += 1

            if i % 100000 == 0:
                print(i)

        plt.plot(real(orbit_tracking), imag(orbit_tracking), linewidth=4, color='orange')
        print(f'Iteration Amount: {i}, \nAngle Summed f1: {step_calc_f1}, \nAngle Summed f2: {step_calc_f2},\nAngle Summed f3: {step_calc_f3}')      
        
        plt.text(0.3, 0.2, r'$P_{1,+,f_\varepsilon}$', fontsize=16, zorder=4)
        plt.text(-0.4, 0.45, r'$P_{1,-,f_\varepsilon}$', fontsize=16, zorder=4)
        plt.text(-0.5, -0.25, r'$P_{2,+,f_\varepsilon}$', fontsize=16, zorder=4)
        plt.text(0.225, -0.4, r'$P_{2,-,f_\varepsilon}$', fontsize=16, zorder=4)

        plt.text(0.125, 0.08, r'$\sigma_1$', fontsize=16, zorder=100)
        plt.text(-0.1, 0, r'$\sigma_0$', fontsize=16, zorder=100)
        plt.text(-0.22, -0.1, r'$\sigma_2$', fontsize=16, zorder=100)

    def plot(self):
        plt.xlim(-self.neighbourhood, self.neighbourhood)
        plt.ylim(-self.neighbourhood, self.neighbourhood)
        plt.gca().set_aspect('equal', adjustable='box')
        plt.axhline(0, color='black', linewidth=.5, zorder=0)
        plt.axvline(0, color='black', linewidth=.5, zorder=0)
        plt.savefig(f'E:\VSCode\mandel-python\\irrational_petal_dynamic.png', bbox_inches='tight', pad_inches=0, dpi=500)
        #plt.savefig(f'E:\VSCode\mandel-python\primitive_bifurvative_intersections\\1_2_bifurcative.png', bbox_inches='tight', pad_inches=0, dpi=500)
        plt.show()

    def gif_q_iter(self, q, iter):
        for i in range(iter + 1):
            n = q + i/20
            self.p = n
            self.plot_fixpoints()
            self.vector_field()
            plt.gca().set_aspect('equal', adjustable='box')
            plt.xlim(-1.5, 1.5)
            plt.ylim(-1.5, 1.5)
            plt.savefig(f'E:\VSCode\mandel-python\gif_vector_field\{str(i).zfill(4)}.png', dpi=500)
            plt.clf()
            print(f'{i}/{iter}')

# CHANGE THE P/Q THING




field = Field(
    p = np.sqrt(21) + 1,
    q = 1,
    eps = 0.1,
    orbit_amount = 30,
    neighbourhood = 0.75,
)
#field.gif_q_iter(5, 200)
#field.plot_fixpoints()
#field.p_3_analysis()
#field.third_pi_point()
field.vector_field()
field.newton_approximate_roots(4)
field.second_pi_point()
#field.vector_field_irrational()
#field.second_pi_point()
#field.newton_approximate_roots(2)
#field.orbit(-(-1/4 * np.sqrt(-6j*np.sqrt(3) + 6) + 1/2))
#field.orbit(-1/2 * np.sqrt(2) + 1/2)
#field.orbit(0)
#field.orbit((-1/2*1j*np.sqrt(2)))
#field.orbit_plot(-0.011608856395105857+0.5776025346989935j)
#field.orbit_plot(-0.011608856395105857+0.5776025346989935j)
#field.petal_plot()
field.plot()

