from PIL import Image
import matplotlib.pyplot as plt
import numpy as np

real = lambda list: [element.real for element in list]
imag = lambda list: [element.imag for element in list]

p = lambda z, c: z**2 + c
#p = lambda z, lam: lam*z*(1 - z)

class escape_time():
    def __init__(self, xmin, xmax, ymin, ymax, pixel_density, relevant_point):
        self.xmin = xmin 
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax
        self.pixel_density = pixel_density #pixels / 1 unit
        self.relevant_point = relevant_point
        
        #self.img = Image.new('RGB', (self.x_range_pixel, self.y_range_pixel), color='black')
        #self.pixel = self.img.load()

    def coloring(self, i):
        #color = ((int(256*(i/MAX_ITER))), int(64*((1-i/MAX_ITER))), int(128*((i/MAX_ITER))))
        color = (int(64*i), 0, 0)
        return color

    def iter(self, z):
        index = 0
        c = z

        # in c coordinates
        z = 0
        # in lambda coordinates
        #z = 1/2
        while index < MAX_ITER and abs(z) < 10:
            z = p(z, c)
            index += 1
        #print(index)
        return index

    def plot_mset(self):
        print('plotting mset...')

        x_range_pixel = int((self.xmax-self.xmin) * self.pixel_density)
        y_range_pixel = int((self.ymax-self.ymin) * self.pixel_density)

        self.img = Image.new('RGB', (x_range_pixel, y_range_pixel), color='black')
        self.pixel = self.img.load()

        for row in range(x_range_pixel):
            for column in range(y_range_pixel):
                zx = ((self.xmax-self.xmin)/x_range_pixel)*row + self.xmin
                zy = ((self.ymax-self.ymin)/y_range_pixel)*column - self.ymax
                z = complex(zx, zy)

                if self.iter(z) == MAX_ITER:
                    self.pixel[row, column] = (256, 256, 256)
            if int(100*row/x_range_pixel) % 5 == 0:
                print(f'{100*row/x_range_pixel}')        


    def escape_time_radius(self):
        orbit_list, orbit_list_iter = [], []

        i_amount = 1000
        mset_flag = False
        for k in range(2, 10):
            radius = 10**(-k)
            for i in range(i_amount):
                orbit_list.append(radius*np.exp(2*1j*np.pi*(i/i_amount)) + self.relevant_point)
                
            # plot mset
            if mset_flag:
                self.xmin = min([j.real for j in orbit_list])
                self.xmax = max([j.real for j in orbit_list])
                self.ymin = min([j.imag for j in orbit_list])
                self.ymax = max([j.imag for j in orbit_list])
                print(self.xmin, self.xmax, self.ymin, self.ymax)
                self.plot_mset()
                self.img.save('mset_escape_time.png')
                plt.xlim(self.xmax, self.xmin)
                plt.ylim(self.ymin, self.ymax)
                plt.gca().set_aspect('equal', adjustable='box')
                image_data = plt.imread('mset_escape_time.png')
                plt.imshow(image_data, extent=[self.xmin, self.xmax, self.ymin, self.ymax])
                mset_flag = False
                #print(orbit_list)

            plt.plot(real(orbit_list), imag(orbit_list))
            
            for i, z in enumerate(orbit_list):
                minimum = MAX_ITER
                index = 0
                c = z

                # in c coordinates
                z = 0
                # in lambda coordinates
                #z = 1/2
                while index < MAX_ITER and abs(z) < 10 and minimum > index:
                    z = p(z, c)
                    index += 1

                if minimum > index:
                    #print(index, abs(z))
                    minimum = index

                orbit_list_iter.append(index)
                #print(i/len(orbit_list))
            
            #if minimum == MAX_ITER:
            #    break

            for i in range(1):

                smallest = orbit_list_iter.index(min(orbit_list_iter))

                print(f"N(ε) = {orbit_list_iter[smallest]}; ε = 10^-{k}; q = {(radius**(1/1) * orbit_list_iter[smallest])/np.pi}")

                plt.scatter(orbit_list[smallest].real, orbit_list[smallest].imag)
                #del orbit_list[smallest]
                #del orbit_list_iter[smallest]
            
            orbit_list, orbit_list_iter = [], []
        


        #plt.show()
                  
    def escape_time_plot(self):
            for row in range(self.x_range_pixel):
                for column in range(self.y_range_pixel):
                    zx = ((self.xmax-self.xmin)/self.x_range_pixel)*row + self.xmin
                    zy = ((self.ymax-self.ymin)/self.y_range_pixel)*column - self.ymax
                    z = complex(zx, zy)

                    iter_amount = self.iter(z)
                    if iter_amount == MAX_ITER:
                        self.pixel[row, column] = (256, 256, 256)
                    else:
                        self.pixel[row, column] = self.coloring(iter_amount*(abs(self.relevant_point - z))**(1/2))
    
    def show(self):
        self.img.show()

MAX_ITER = 10000

tests = escape_time(
    xmin = -1.8, xmax = -1.7,
    ymin = 0, ymax = 0.1,
    pixel_density = 500,
    relevant_point = np.exp(2*np.pi*1j*(1/4)),
)


tests.escape_time_radius()

