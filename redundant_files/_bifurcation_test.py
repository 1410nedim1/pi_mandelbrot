import numpy as np
import matplotlib.pyplot as plt


#define the inverse jacobi function
inv_jacobi = lambda z, c: np.linalg.inv(np.array([[2*z-1, 1], 
                                                  [2*z/abs(z), 0]], 
                                                   dtype=np.cdouble))

# define function vector
func_vec = lambda z, c: np.array([[z**2 - z + c], 
                                  [abs(2*z)-1]], 
                                  dtype=np.cdouble)

loc_vector = lambda z, c: np.array([[z], 
                                    [c]], 
                                    dtype=np.cdouble)

x_n = loc_vector(-1, 0.1)

for i in range(1000):
    funcky = func_vec(x_n[0,0], x_n[1,0])

    z = x_n[0,0]
    c = x_n[1,0]
            
    x_n = x_n - np.dot(inv_jacobi(x_n[0,0], x_n[1,0]), funcky)

print(x_n)