import matplotlib.pyplot as plt
import numpy as np

z = lambda zeta, q: (-q*zeta)**(-1/q)
real_part = lambda list: [element.real for element in list]
imaginary_part = lambda list: [element.imag for element in list]

class Ecalle():
    def __init__(self, eps, p, q, start_orbit_amount, iteration_amount, orbit_main_attracting = [], orbit_main_repelling = [], start_orbit_axis = 10):
        self.eps = eps
        self.p = p
        self.q = q
        self.start_orbit_amount = start_orbit_amount
        self.iteration_amount = iteration_amount
        self.orbit_main_repelling = orbit_main_repelling
        self.orbit_main_attracting = orbit_main_attracting
        self.start_orbit_axis = start_orbit_axis

    def place_and_iterate(self):
        orbit_repelling, orbit_attractive = [], []
        # Place starting orbits        
        for j in range(2*self.start_orbit_amount):
            try: 
                orbit_repelling.append(z(complex(-500, self.start_orbit_axis*(j/self.start_orbit_amount) - self.start_orbit_axis), (self.p/self.q)-1))
            except ZeroDivisionError:
                print('ZeroDivisionError')
                pass

        for j in range(2*self.start_orbit_amount):
            try: 
                orbit_attractive.append(z(complex(4, self.start_orbit_axis*(j/self.start_orbit_amount) - self.start_orbit_axis), (self.p/self.q)-1))
            except ZeroDivisionError:
                print('ZeroDivisionError')
                pass
        

        print('Iterating Attractive')
        flag_rep = True
        while flag_rep:
            try:
                orbit_attractive = list(map(lambda z: z**(self.p/self.q) + z + self.eps, orbit_attractive))
                if max([i.real for i in orbit_attractive]) > -0.0001:
                    flag_rep = False
                else:
                    self.orbit_main_attracting.append(orbit_attractive)
            except OverflowError:
                print('OverflowError')
                break
        
        print('Iterating Repelling')
        flag_att = True
        while flag_att:
            try:
                orbit_repelling = list(map(lambda z: z**(self.p/self.q) + z + self.eps, orbit_repelling))
                if max([i.real for i in orbit_repelling]) > 0.2:
                    flag_att = False
                else:
                    self.orbit_main_repelling.append(orbit_repelling)
            except OverflowError:
                print('OverflowError')
                break
        
    def connect_iteration(self, nthiteration):    
        print('Starting to connect iterations')
        for i in range(0, len(self.orbit_main_attracting), nthiteration):
            plt.plot(real_part(self.orbit_main_attracting[i]), imaginary_part(self.orbit_main_attracting[i]), color='blue', zorder=1)

        for i in range(0, len(self.orbit_main_repelling), nthiteration):
            plt.plot(real_part(self.orbit_main_repelling[i]), imaginary_part(self.orbit_main_repelling[i]), color='red', zorder=1)



    def connect_orbit(self, nthiteration):
        print('Starting to connect orbits')
        for i in range(len(self.orbit_main[0])):
            temp = []
    
            if i % nthiteration == 0:
                for sublist in self.orbit_main:
                    temp.append(sublist[i])
            
                plt.plot(real_part(temp), imaginary_part(temp), zorder=2)

print('Initializing')
ecalle = Ecalle(
    eps = 0,
    p = 3,
    q = 1,
    start_orbit_amount = 1000,
    iteration_amount = 500,
    start_orbit_axis= 100
)   
ecalle.place_and_iterate()
ecalle.connect_iteration(1)

plt.gca().set_aspect('equal')
plt.scatter(0,0, color='green')

plt.savefig('ecallecylinder.png', dpi=500)
plt.show()