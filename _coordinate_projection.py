import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle # To draw the empiric ecalle thing
import numpy as np
import colorsys

z = lambda zeta, q: (-q*zeta)**(-1/q)
fixpoint_calc = lambda p, q, eps, i: abs(eps)**(q/p) * np.exp(np.pi*1j*(2*i + 1)*(q/p))
derivative_iteration = lambda p, q, z: 1 - (p/q)*z**((p/q)-1)
real_part = lambda list: [element.real for element in list]
imaginary_part = lambda list: [element.imag for element in list]
degree_calc = lambda q, eps: (q+1)*eps**(q/(q+1)) * np.sin(np.pi*q/(q+1))

class Ecalle():
    def __init__(self, eps, p, q, start_orbit_amount, iteration_amount, positiv_axis_iter, repelling_flag, attractive_flag, orbit_central_flag):
        self.eps = eps
        self.p = p
        self.q = q
        self.start_orbit_amount = start_orbit_amount
        self.iteration_amount = iteration_amount
        self.positiv_axis_iter = positiv_axis_iter
        self.repelling_flag = repelling_flag
        self.attractive_flag = attractive_flag
        self.orbit_central_flag = orbit_central_flag
        self.orbit_main = []

    # This plots the fixedpoints and everything about them.
    def plot_fixpoints(self):

        fixpoints_list = []
        attractiveness = []

        for i in range(100):
            # All fixedpoints with an argument in the principal branch arg(z) ∈ (-π, π]

            if ((2*i) + 1) * (self.q/self.p) > 1:
                break

            fixpoint = fixpoint_calc(self.p, self.q, self.eps, i)
            fixpoints_list.append(fixpoint)
            fixpoints_list.append(np.conjugate(fixpoint))
            attractiveness.append(abs(derivative_iteration(self.p, self.q, fixpoint)))
            attractiveness.append(abs(derivative_iteration(self.p, self.q, np.conjugate(fixpoint))))
            

        for i in range(len(attractiveness)):
            if attractiveness[i] > 1: # Red is repulsive    
                plt.scatter(fixpoints_list[i].real, fixpoints_list[i].imag, color='green', zorder=4)
            elif attractiveness[i] < 1: # Blue is attractive
                plt.scatter(fixpoints_list[i].real, fixpoints_list[i].imag, color='blue', zorder=4)

        #plt.scatter(0, 0, color='black', zorder=3)

    # Place the starting orbits
    def plot_starting_orbits(self, fix_a, fix_b):
        orbit = []
        print(fix_a, fix_b)
        # Place starting orbits        
        for j in range(self.start_orbit_amount):
            #orbit.append(z(complex(-10, j*100), (self.p/self.q)-1))
            # this is works. Dont touch it: orbit.append(complex(fix_a.real, (fix_a.imag - fix_b.imag) * j / (2*self.start_orbit_amount)))
            orbit.append(fix_b + (fix_a - fix_b + (fix_a - fix_b)/self.start_orbit_amount) * (j / self.start_orbit_amount))
        return orbit

    # The most important function. It places and iterates over a set of points. 
    def place_and_iterate(self, gif_make=False):
        
        # Usually n=0 should work as these are the fixedpoints closest to the real axis
        n = 0
        fix_a = fixpoint_calc(self.p, self.q, self.eps, n)
        fix_b = fixpoint_calc(self.p, self.q, self.eps, n-1)

        # Initializes the placement of starting orbits and seperates them into attractive and repelling lists
        orbit = self.plot_starting_orbits(fix_a, fix_b)
        orbit_attractive = orbit
        orbit_repelling = orbit

        if self.attractive_flag:
            print('Iterating Attractive')
            orbit_attractive_temp = []
            orbit_central = (fix_a + fix_b)/2
            iter = 0
            orbit_attractive_temp.append(orbit_attractive)
            # orbit_central.real > 0
            while abs(orbit_central) < self.positiv_axis_iter:
                try:
                    orbit_attractive = list(map(lambda z: z - z**(self.p/self.q) - self.eps, orbit_attractive))
                    orbit_attractive_temp.append(orbit_attractive)
                    orbit_central = orbit_central - orbit_central**(self.p/self.q) - self.eps
                    
                    if self.orbit_central_flag:
                        plt.scatter(orbit_central.real, orbit_central.imag, color='green')
                    iter += 1
                except OverflowError:
                    iter += 1
                    print('OverflowError')
                    break

        if self.repelling_flag:
            print('Iterating Repelling')
            orbit_repelling_temp = []
            orbit_central = (fix_a + fix_b)/2
            iter = 0
            # orbit_real < self.positiv_axis_iter
            # iter < (np.pi-0.5)/degree_calc(self.p/self.q, self.eps) and orbit_real < self.positiv_axis_iter
            # abs(orbit_central) < self.positiv_axis_iter and abs(orbit_central) > 0 and iter < self.iteration_amount
            while abs(orbit_central) < self.positiv_axis_iter and orbit_central.real > 0:
                try:
                    orbit_repelling = list(map(lambda z: z + z**(self.p/self.q) + self.eps, orbit_repelling))
                    orbit_repelling_temp.append(orbit_repelling)
                    orbit_central = orbit_central + orbit_central**(self.p/self.q) + self.eps
                    
                    if self.orbit_central_flag:
                        plt.scatter(orbit_central.real, orbit_central.imag, color='orange')
                    iter += 1
                except OverflowError:
                    iter += 1
                    print('OverflowError')
                    break

        #print(f'{np.pi*len(orbit_repelling_temp)/ (len(orbit_repelling_temp) + len(orbit_attractive_temp))}')

        # This is solely for the gif function, because python sucks at ordering things :(
        for sublist in orbit_repelling_temp[::-1]:
            self.orbit_main.append(sublist)
        for sublist in orbit_attractive_temp:
            self.orbit_main.append(sublist)

    # This creates a color palette based on the 
    def generate_color_palette(self, num_colors, direction):
        base_hue = 180.0 / 360.0  # Green
        
        color_palette = []
        for i in range(num_colors):
            hue = (base_hue + direction*(i / num_colors) * 1) % 1.0
            rgb_color = colorsys.hsv_to_rgb(hue, 1.0, 1.0)
            hex_color = '#{:02x}{:02x}{:02x}'.format(int(rgb_color[0]*255), int(rgb_color[1]*255), int(rgb_color[2]*255))
            color_palette.append(hex_color)
        return color_palette
    
    # This connects every single iteration
    # iter_select goes from the first iteration being the last element of the list to the first element being the last iteration
    def connect_iteration(self, gif_make=False, nthiteration=1, iter_select_flag=False, iter_select=None):    
        print('Starting to connect iterations')    
        colors = self.generate_color_palette(len(self.orbit_main), -1)
        
        if iter_select_flag:
            plt.plot(real_part(self.orbit_main[iter_select]), imaginary_part(self.orbit_main[iter_select]), color='black', zorder=2)
        else:
            for i in range(0, len(self.orbit_main), nthiteration):
                plt.plot(real_part(self.orbit_main[i]), imaginary_part(self.orbit_main[i]), color=colors[i], zorder=2)
                
                if gif_make:
                    img_number = str(i).zfill(4)
                    plt.savefig(f'E:\VSCode\mandel-python\gif_ecalle_img\{img_number}.png', dpi=500)
                print(100*i/len(self.orbit_main))

    # This connects every single orbit
    def connect_orbit(self, nthiteration=1):
        print('Starting to connect orbits')
        for i in range(len(self.orbit_main[0])):
            temp = []
    
            if i % nthiteration == 0:
                for sublist in self.orbit_main:
                    temp.append(sublist[i])
                plt.plot(real_part(temp), imaginary_part(temp), zorder=2)

    # This doesnt work as intended. Needs fix
    def half_circle(self, center, start_point):
        
        start_angle = np.arctan2(start_point.imag - center.imag, start_point.real - center.real)
        theta = np.linspace(start_angle, start_angle + np.pi, 100)

        radius = abs(abs(center) - abs(start_point))*3
        x = radius * np.cos(theta) + center.real
        y = radius * np.sin(theta) + center.imag

        plt.plot(x, y, linewidth=3, color='black')

    def orbit(self, z):
        orbit_list = []
        while abs(z) < 0.55:
            orbit_list.append(z)
            z = z + z**(self.p/self.q) + self.eps
        orbit_list.append(z)
        plt.plot(real_part(orbit_list), imaginary_part(orbit_list), zorder=3, linewidth=4, color='orange')
    
    # This is to plot images that zoom into the Fixedpoint. This is to show the local dynamics
    def empiric_ecalle(self):
        fix_a = fixpoint_calc(self.p, self.q, self.eps, 0)
        f_temp = self.orbit_main[-1][-2]
        plt.axline((fix_a.real, fix_a.imag), (f_temp.real, f_temp.imag), zorder=3, color='black')

        #self.half_circle(fix_a, f_temp) # only when ma = 0.001



        bound_calc = fixpoint_calc(self.p, self.q, self.eps, 0)
        plt.gca().add_patch(Rectangle((bound_calc.real - (0.04), bound_calc.imag - (0.04)),0.04*2, 2*0.04, linewidth=2,edgecolor='black',facecolor='none', zorder=4))

        ma = 0.3
        mi = ma

        plt.gca().set_xlim([bound_calc.real - 1*mi, bound_calc.real + 1*ma])
        plt.gca().set_ylim([bound_calc.imag - mi, bound_calc.imag + ma])
        plt.gca().set_aspect('equal')
        plt.savefig(f'E:\VSCode\mandel-python\q_{self.p/self.q}_ecallecyllinder_{self.eps}_{0.1/ma}_zoom.png', bbox_inches='tight', pad_inches=0.25, dpi=500)

    # I dont know if this is still needed
    def set_aspect_fixed_points(self):
        fix_a = fixpoint_calc(self.p, self.q, self.eps, 0)
        plt.gca().set_xlim([-10*fix_a.imag, 10*fix_a.imag])
        plt.gca().set_ylim([-10*fix_a.imag, 10*fix_a.imag])
    
    # This creates a gif of each mapping of Ecalle-Cyllinders. Every picture is saved in a folder
    def maker_gif(self):
        self.plot_fixpoints()
        self.place_and_iterate(gif_make=True)
        self.connect_iteration(gif_make=True, nthiteration=1)
        plt.gca().set_aspect('equal')
        plt.clf()
        #plt.show()

    # TEMPORARY VECTOR FIELD THING
    def disgusting(self):
        def complex_grid():
            real_range = np.linspace(-0.55, 0.55, 22)
            imag_range = np.linspace(-0.55, 0.55, 22)
            real_part, imag_part = np.meshgrid(real_range, imag_range)
            complex_grid = real_part + 1j * imag_part
            unchained = [num for sublist in complex_grid for num in sublist]
            return unchained

        # Orbits are placed on a complex grid in the given neigbourhood and iterated over.
        def place_and_iter(vector_field=False, iterate_attr=True):
            c_temp = complex_grid()

            if vector_field:
                complex_grid_repelling_prime = list(map(lambda z: z + z**(self.p/self.q) + self.eps, c_temp))
                complex_grid_attractive_prime = list(map(lambda z: z - z**(self.p/self.q) - self.eps, c_temp))
                if iterate_attr:
                    return [complex_grid_attractive_prime + c_temp, c_temp + complex_grid_repelling_prime]
                else:
                    return [c_temp, complex_grid_repelling_prime]
            else:
                return c_temp
        
        
        def lines_on_a_map():
            
            orbit_main = []
            orbit_main = place_and_iter(True, False)

            for i in range(len(orbit_main) - 1):
                first_iter = orbit_main[i]
                second_iter = orbit_main[i + 1]
                #print(first_iter)
                for a, b in zip(first_iter, second_iter):
                    plt.quiver(a.real, a.imag, 
                            b.real - a.real, b.imag - a.imag,
                            color='black', width=0.004,
                            alpha = 0.35
                            )
        lines_on_a_map()
    
    
    def plot(self):
        plt.xlim(-0.55, 0.55)
        plt.ylim(-0.55, 0.55)
        plt.gca().set_aspect('equal', adjustable='box')
        #plt.axhline(0, color='black', linewidth=.5)
        #plt.axvline(0, color='black', linewidth=.5)
        plt.savefig(f'E:\VSCode\mandel-python\\coordinate_projection.png', bbox_inches='tight', pad_inches=0, dpi=500)
        plt.show()


'''ecalle = Ecalle(
    eps = 0.001,
    p = 3,
    q = 1,
    start_orbit_amount = 1000,
    iteration_amount = 100,
    positiv_axis_iter = 2,
)   
ecalle.maker_gif()'''

ecalle = Ecalle(
        eps = 0.01,
        p = 2,
        q = 1,
        start_orbit_amount = 1000,
        iteration_amount = 1000,
        positiv_axis_iter = 0.6,
        repelling_flag = True,
        attractive_flag = True,
        orbit_central_flag = False,
)   

plt.text(0.3, 0.05, r'$P_{1,+,f}$', fontsize=16)
plt.text(-0.45, 0.05, r'$P_{1,-,f}$', fontsize=16)

plt.text(0, 0.05, r'$\sigma_+$', fontsize=16)
plt.text(0, -0.15, r'$\sigma\_$', fontsize=16)
plt.xticks(visible=False)
plt.yticks(visible=False)


ecalle.plot_fixpoints()
ecalle.place_and_iterate()
ecalle.connect_iteration(nthiteration = 1,
                         iter_select_flag = False,
                         iter_select = 0)
#ecalle.empiric_ecalle()
ecalle.orbit(-1/2)
ecalle.disgusting()
ecalle.plot()