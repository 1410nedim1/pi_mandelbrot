from PIL import Image
import matplotlib.pyplot as plt
import numpy

class JuliaSet:

    def __init__(self, c, size, iterative, perturbation, eps, orbit_compare, extent=3.0, max_iter_image=500, max_iter = 10, g_image=False):
        self.c = c
        self.size = size
        self.extent = extent
        self.max_iter = max_iter
        self.max_iter_image = max_iter_image
        self.image = Image.new('RGB', (self.size, self.size), color='black')
        self.pixels = self.image.load()
        self.g_image = g_image
        self.iterative = iterative 
        self.perturbation = perturbation
        self.eps = eps

    def generate_image(self):
        print('started generating')

        orbit_compare_list = []

        for x in range(self.size):
            zx = (x * self.extent / self.size) - (self.extent / 2)
            for y in range(self.size):
                zy = (y * self.extent / self.size) - (self.extent / 2)

                iteration = 0
                z = complex(zx, zy)
                c = z
                 
                flag = False
                if abs(z - 0.5j) < 0.01:
                    flag = True
                
                while abs(z) < 2 and iteration < self.max_iter_image:
                    if flag:
                        plt.scatter(z.real, z.imag)
                    try:
                        z = self.iterative(z) + self.perturbation(z, self.eps)    
                    except OverflowError:
                        print('OverflowError')
                        break
                    iteration += 1
                flag = False

                
                # colorize the pixels 
                # Modulo coloring - nice colors ;)
                # color = (iteration % 32 * 8, iteration % 16 * 16, iteration % 8 * 32)

                # Boring linear coloring *urggghhhh* - oscar likes it though
                gray_scale = int(256*iteration/self.max_iter_image)
                color = (gray_scale, gray_scale, gray_scale)
                self.pixels[x, y] = color
            
            if x > 0 and x % 10==0:
                print(f'{100*x/self.size}%')
        
        # Plot reference orbit
        print(orbit_compare_list, len(orbit_compare_list))
        real = [z.real for z in orbit_compare_list]
        imag = [z.imag for z in orbit_compare_list]
        plt.plot(real, imag, marker='o')

        self.g_image = True

    def save_image(self):
        if self.g_image:
            #self.image.show() 
            self.image.save('julia_set.png')
            print('saved')
        else:
            print('no image generated - cant save')
    def img_plot(self):
        plt.imshow(plt.imread("E:\VSCode\mandel-python\julia_set.png"), extent=[-self.extent/2, self.extent/2, -self.extent/2, self.extent/2])
        plt.xlim(-self.extent/2, self.extent/2)
        plt.ylim(-self.extent/2, self.extent/2)

    def plot_orbit(self, z):
        orbit, fixed_point_list = [], []
        derivative_iterative = lambda z, eps: 1 + 3*z**2 # + 2*2**0.5 * eps*z + 2*eps*1j
        degree_sum = 0

        for _ in range(self.max_iter):    
            orbit.append(z)        
            if abs(z) > 2.0:
                break
            pert = self.perturbation(z, self.eps)
            z = self.iterative(z) + pert

            # add fixed points to list - because fixed points are dependant on z
            a = self.fixed_point_plot(pert, 0)
            b = self.fixed_point_plot(pert, 1)
            c = self.fixed_point_plot(pert, 2)
            fixed_point_list.append((a, b, c))

            # degree summation
            degree_sum += numpy.angle(derivative_iterative(a, self.eps), deg=False)
        
        print(f'The sum of every step angle is: {degree_sum/numpy.pi}')
        
      
        '''# plot fixed point
        for a, b, c in fixed_point_list:
            plt.scatter(a.real, a.imag, color='red', zorder=3)
            plt.scatter(b.real, b.imag, color='black', zorder=3)
            plt.scatter(c.real, c.imag, color='blue', zorder=3)

       
        # first fixed point
        plt.scatter(fixed_point_list[0][0].real, fixed_point_list[0][0].imag, color='yellow', zorder=4)
        plt.scatter(fixed_point_list[0][1].real, fixed_point_list[0][1].imag, color='yellow', zorder=4)
        plt.scatter(fixed_point_list[0][2].real, fixed_point_list[0][2].imag, color='yellow', zorder=4)'''
        
        
        # plot stuff
        plt.xlabel("real")
        plt.ylabel("imaginary")
        

        plt.axhline(0, color='black', linewidth=.5)
        plt.axvline(0, color='black', linewidth=.5)


        real = [z.real for z in orbit]
        imag = [z.imag for z in orbit]
        plt.plot(real, imag, marker='o')

    def fixed_point_plot(self, pert, k):
        return abs(pert)**(1/3) * numpy.exp(1j*((numpy.angle(pert) + numpy.pi + 2*numpy.pi*k)/3))

# This class plots a fractal with the polynomial given in 'iterative'. 
# It can then be used to plot orbits over the fractal
julia = JuliaSet(
                 size = 800, 
                 extent = 4, 
                 c = 0, 
                 max_iter_image = 1000,
                 max_iter = 1000, 
                 iterative = lambda z: z**3 + z,
                 perturbation = lambda z, eps: (2**0.5)*eps*(z**2) + 2j*eps*z + eps**2,
                 eps = 0.01,
                 orbit_compare = 0.1+0.5j,
                 )
julia.generate_image()
julia.save_image()
julia.img_plot()

julia.plot_orbit(-0.6j)

plt.scatter(0, 1/(3**0.5), color='black')
plt.scatter(0, -1/(3**0.5), color='black')

plt.show()

# z**4 Term: - ((1/8)**0.5)*(z**4)*1j