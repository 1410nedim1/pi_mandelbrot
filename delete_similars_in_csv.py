import csv
import math

file_path = './bifurcation_points.csv'

data = []
with open(file_path, mode='r', newline='', encoding='utf-8') as file:
    reader = csv.reader(file)
    for row in reader:
        data.append(complex(row[0]))
print(data)

# Algorithm to remove duplicates
# It first sorts the complex numbers after its real values and then checks if neighboring numbers in the list are close to each other.
# If the numbers are close to each other it removes the first

'''
deletion_counter = 0
data = sorted(data, key=lambda x: x.real)
i = 0
while i < len(data) - 1:
    a = data[i]
    b = data[i+1]
    
    print(abs(a - b))
    if abs(a - b) < 1e-15:
        del data[i]
        deletion_counter += 1
    else:
        i += 1
'''

result = list(set(round(x.real, 10) + round(x.imag, 10) * 1j for x in data))
result = sorted(result, key=lambda x: x.real)

with open(file_path, mode='w', newline='', encoding='utf-8') as file:
    writer = csv.writer(file)
    for element in result:
        writer.writerow([str(element)])

print('removed similar items in csv')
