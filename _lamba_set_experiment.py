from PIL import Image
import numpy as np
import matplotlib.pyplot as plt

class lamdada:
    def __init__(self) -> None:
        pass

    def generate_set(self):
        pixel_dimension = 2000
        xmin, xmax = -1.75, 3.75
        ymin, ymax = -2.75, 2.75
        p = lambda z, lam: lam * z * (1-z)
        z = 1/2
        MAX_ITER = 1000

        img = Image.new('RGB', (pixel_dimension, pixel_dimension), color='black')
        pixel = img.load()

        def coloring(i):
            color = ((i * 5) % 256, (i * 10) % 256, (i * 20) % 256)
            return color

        def iter_function(lam, MAX_ITER):
            index = 0
            z = 1/2
            while index < MAX_ITER and abs(z) < 2:
                z = p(z, lam)
                index += 1
            return index

        for row in range(pixel_dimension):
            zx = xmin + (row/pixel_dimension) * (xmax - xmin)
            for column in range(pixel_dimension):
                zy = ymin + (column/pixel_dimension) * (ymax - ymin)
                
                lam = complex(zx, zy)

                iteration_count = iter_function(lam, MAX_ITER)

                if iteration_count < MAX_ITER:
                    color = coloring(iteration_count)
                    pixel[row, column] = color
            print(row/pixel_dimension)
        img.save('lambda.png')
        img.show()

    def count_iterations(self):
        l = lambda z, lam: lam * z * (1-z)

        def iter_function(lam):
            index = 0
            z = 1/2
            while index < 100000000 and abs(z) < 2:
                z = l(z, lam)
                index += 1
            return index
        
        for k in range(10):
            print(iter_function(1+2*np.sqrt(1-10**(-k)*1j)))
            #plt.scatter(k, iter_function(3 - 10**(-k)*1j))

lad = lamdada()
lad.generate_set()
#lad.count_iterations()
plt.show()