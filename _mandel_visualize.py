from PIL import Image
import matplotlib.pyplot as plt

real = lambda list: [element.real for element in list]
imag = lambda list: [element.imag for element in list]

pixel_dimension = 4000
xmin, xmax = -2, 0.48
ymin, ymax = -1.24, 1.24
p = lambda z, c: z**2 + c
MAX_ITER = 1000

'''
img = Image.new('RGB', (pixel_dimension, pixel_dimension), color='black')
pixel = img.load()

def coloring(i):
    color = ((i * 5) % 256, (i * 10) % 256, (i * 20) % 256)
    return color

def iter(z, MAX_ITER):
    index = 0
    c = z
    while index < MAX_ITER and abs(z) < 2:
        z = p(z, c)
        index += 1
    return index

for row in range(pixel_dimension):
    zx = xmin + (row/pixel_dimension) * (xmax - xmin)
    for column in range(pixel_dimension):
        zy = ymin + (column/pixel_dimension) * (ymax - ymin)
        z = complex(zx, zy)

        iteration_count = iter(z, MAX_ITER)

        if iteration_count < MAX_ITER:
            color = coloring(iteration_count)
            pixel[row, column] = color
    print(row/pixel_dimension)


img.save('proto_mandelbrot.png')'''
#img.show()


plt.imshow(plt.imread("proto_mandelbrot.png"), extent=[xmin, xmax, ymin, ymax])
plt.xlim(xmin, xmax)
plt.ylim(ymin, ymax)
'''
plt.arrow(-0.75, ymin, 0, ymax-0.05, head_width=0.025, head_length=0.025, fc='red', ec='red', zorder=5)
plt.arrow(-0.75, ymax, 0, ymin+0.05, head_width=0.025, head_length=0.025, fc='red', ec='red', zorder=5)

plt.arrow(xmax, 0, -0.25+0.075, 0, head_width=0.025, head_length=0.025, fc='red', ec='red', zorder=5)
'''

plt.plot([-3/4, -3/4], [ymin, ymax], color='red', linewidth=2)
plt.plot([xmax, 1/4], [0, 0], color='red', linewidth=2)


orbit_list = []
for i in range(-100, 100):
    orbit_list.append(-(i/20)**2 + i/20 * 1j - 5/4) 
plt.plot(real(orbit_list), imag(orbit_list), color='red', linewidth=2)

plt.scatter(0.25, 0, color='green', zorder=4)
plt.scatter(-0.75, 0, color='orange', zorder=4)
plt.scatter(-5/4, 0, color='purple', zorder=4)
p_3 = -1/8 + +0.649519052838329j
plt.scatter(p_3.real, p_3.imag, color='lightblue', zorder=4)


plt.axhline(0, color='white',linewidth=0.5, zorder=0)
plt.axvline(0, color='white',linewidth=0.5, zorder=0)

plt.text(-0.7, 0.04, r'$-3/4$', color='white')
plt.text(0.15, -0.12, r'$1/4$', color='white')
plt.text(-1.25, 0.012, r'$-5/4$', color='white')
plt.text(-0.18, 0.525, r'$B_{1, 3}$', color='white')


plt.savefig('post_mandelbrot.png', bbox_inches='tight', pad_inches=0, dpi=500)
plt.show()
