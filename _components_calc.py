import matplotlib.pyplot as plt
import numpy as np
import sympy as sp

e = lambda i, ma: np.exp(2*np.pi*1j*(i/ma))
c = lambda my: my/2 - (my/2)**2
dmand = lambda z: 2*z
real = lambda list: [element.real for element in list]
imag = lambda list: [element.imag for element in list]

cardioid_flag = True

class Cardioid:
    def __init__(self, i , id) -> None:
        self.i = i
        self.id = id

    def drawing_cardioid(self, ma):
        drawing_cardioid = []
        for i in range(ma):
            point = e(i, ma)
            point = c(point)
            drawing_cardioid.append(point)
        plt.plot(real(drawing_cardioid), imag(drawing_cardioid), color='black')
        
        start_point = c(1)
        plt.scatter(start_point.real, start_point.imag, color='orange')

    def fixedpoint_calc(self, c):
        fix_a = 0.5 + (0.25 - c)**0.5
        fix_b = 0.5 - (0.25 - c)**0.5

        #print(dmand(fix_b))
        plt.scatter(fix_a.real, fix_a.imag, color='green')
        plt.scatter(fix_b.real, fix_b.imag, color='green')

    # Calculation of fixedpoints needs to happen beforehand
    def conjugation(self):
        z = sp.symbols('z')
        
        f = z**2 + c(e(self .i, self.id))

        iteration_count = 0
        fix_a = 0.5 + (0.25 - c(e(self.i, self.id)))**0.5
        fix_b = 0.5 - (0.25 - c(e(self.i, self.id)))**0.5

        if np.isclose(1, abs(dmand(fix_a))):
            z_s = fix_a
        elif np.isclose(1, abs(dmand(fix_b))):
            z_s = fix_b

        #print(np.angle(dmand(z_s)))

        phi = z - z_s
        phi_r = z + z_s

        simpl = sp.expand(f.subs(z, phi_r)) - z_s
        #print(simpl)

        simpler = simpl
        for _ in range(self.id-1):
            simpler = simpler.subs(z, simpl)
        simpler = sp.expand(simpler)
        
        #print(sp.expand(simpler, tolerance=1e-8))

    def all_component(self, ma):
        for n in range(1, ma + 1):
            for j in range(1, n):
                point = c(e(j, n))
                plt.scatter(point.real, point.imag, color='blue')

    def select_component(self):
        point = c(e(self.i, self.id))
        print("Roter Punkt",point)
        #plt.scatter(point.real, point.imag, color='red', zorder=4)

        self.fixedpoint_calc(point)

'''
for i in range(1, id):
    if cardioid_flag:
        point = c(e(i, id))
        fixpoint_calc(point)
        print(point)
    else:
        point = e(i, id)
    plt.scatter(point.real, point.imag, color='blue')
'''

card = Cardioid(
    i = 1,
    id = 3,
)
card.drawing_cardioid(200)
card.all_component(ma = 10)
card.select_component()
card.conjugation()


#plt.gca().set_aspect('equal', adjustable='box')
#plt.show()