import csv
import matplotlib.pyplot as plt
import numpy as np


class Analysis:
    def __init__(self) -> None:
          self.data = []
     
    def initialize(self):
        raw_data = []
        with open('aus_ein_analysis.csv', 'r') as file:
            csv_reader = csv.reader(file)
            for row in csv_reader:
                raw_data.append(row)

        print(raw_data[0])
        del raw_data[0]
        
        raw_data = [[float(item) for item in sublist] for sublist in raw_data ]
        
        grouped = {}
        for sublist in raw_data:
                key = sublist[0] 
                if key in grouped:
                    grouped[key].append(sublist)
                else:
                    grouped[key] = [sublist]
        self.data = list(grouped.values())

    def eps_ein(self):

        for q in self.data:
            x_list, y_list = [], []
            for i in q:
                x_list.append(i[2])
                y_list.append(i[6])
            #plt.plot(x_list, y_list, color='blue')    
            #plt.gca().invert_xaxis()
        #plt.xscale('log')
        plt.gca().set_aspect('equal', adjustable='box')
        self.comp(x_list, y_list)
        plt.show()
    
    def eps_aus(self):
        for q in self.data:
            x_list, y_list = [], []
            for i in q:
                x_list.append(i[2])
                y_list.append(i[7])
            plt.plot(x_list, y_list)    
            plt.gca().invert_xaxis()
        plt.xscale('log')
        plt.show()
    
    def eps_gesamt(self):
        for q in self.data:
            x_list, y_list = [], []
            for i in q:
                x_list.append(i[2])
                y_list.append(i[3])
            plt.plot(x_list, y_list)
            plt.gca().invert_xaxis()
        plt.xscale('log')
        plt.gca().set_aspect('equal', adjustable='box')

        plt.show()

    def eps_aus_ein(self):
            for q in self.data:
                x_list, y_list = [], []
                for i in q:
                    x_list.append(i[2])
                    y_list.append(i[8])
                plt.plot(x_list, y_list)    
                plt.gca().invert_xaxis()
            plt.xscale('log')
            plt.show()
    
    def comp(self, eps_list, y_list):
        def f(x):
            return x**-1 - x**2 - 1

        k = []
        for i, y in enumerate(eps_list):
            k.append(np.log10(1/y))

        for i, k in enumerate(k):
            print(k, f(k), y_list[i])
            plt.scatter(k, y_list[i]/f(k))




analysis = Analysis()
analysis.initialize()
analysis.eps_ein()