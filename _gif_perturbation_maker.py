from PIL import Image
import os

def natural_sort(file_list):
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [convert(c) for c in key.split('_')]
    return sorted(file_list, key=alphanum_key)

def create_gif(image_list, gif_name):
    frames = []
    for image_path in image_list:
        frame = Image.open(image_path)
        frames.append(frame)

    # Save into a GIF file that loops forever
    frames[0].save(gif_name, format='GIF', append_images=frames[1:], save_all=True, duration=300, loop=0)

if __name__ == "__main__":
    # Folder containing the images
    folder_path = "gif_vector_field"

    # Get filenames of all image files in the folder
    filenames = [filename for filename in os.listdir(folder_path) if filename.endswith(('.png', '.jpg', '.jpeg', '.gif'))]

    # Sort filenames in natural order
    sorted_filenames = natural_sort(filenames)

    # Get paths of images in the correct order
    image_paths = [os.path.join(folder_path, filename) for filename in sorted_filenames]

    # Name for the output GIF
    gif_name = "q_video.mp4"

    # Create GIF
    create_gif(image_paths, gif_name)
    print('Done.')
