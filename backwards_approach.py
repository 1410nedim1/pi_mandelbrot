import matplotlib.pyplot as plt

class Backturbation:
    def __init__(self, xmin, xmax, ymin, ymax, yresolution, xresolution, f, tolerance) -> None:
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax
        self.yresolution = yresolution
        self.xresolution = xresolution
        self.f = f
        self.tolerance = tolerance

    
    def iteration(self, eps, max_iter):
        z = 0
        for iter in range(max_iter):
            if abs(self.f(z, eps)) > 2:
                return iter
            z = self.f(z, eps)
        return max_iter
    
    def local_approximation(self, eps):
        z = 0
        f_eps = lambda z, eps: z + z**3 + eps
        iter = 0
        
        while abs(z) < 2:
            z = f_eps(z, eps)
            iter +=1
        return iter
    
    def plane(self, e):

        good_iter = self.local_approximation(e)
        print('Locally approximated:', good_iter)

        for row in range(self.xresolution):
            zx = self.xmin + (row/self.xresolution) * (self.xmax - self.xmin)
            for column in range(self.yresolution):
                zy = self.ymin + (column/self.yresolution) * (self.ymax - self.ymin)
                eps = complex(zx, zy)
                
                temp_iter = self.iteration(eps, good_iter + 20)

                if  temp_iter - self.tolerance <= good_iter and temp_iter + self.tolerance >= good_iter:
                    plt.scatter(eps.real, eps.imag)
            
            #print(f'{100*row/self.xresolution}%')



backturbation = Backturbation(
    xmin = -2,
    xmax = 2,
    ymin = -2,
    ymax = 2,
    yresolution = 5000,
    xresolution = 5000,
    f = lambda z, eps: (z**2 - 3/4)**2 - 3/4 + eps,
    tolerance = 0
)
print(backturbation.local_approximation(0.00000001))

plt.gca().set_aspect('equal')
#plt.show()
