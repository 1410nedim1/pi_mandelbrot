import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

# Create a figure and axis
fig, ax = plt.subplots()
x = np.linspace(0, 2 * np.pi, 100)
line, = ax.plot(x, np.sin(x))  # Initial plot (a sine wave)

# Define the function to update the plot
def update(frame):
    line.set_ydata(np.sin(x + frame / 10.0))  # Update the y-data (shifting the sine wave)
    return line,

# Create the animation
ani = FuncAnimation(fig, update, frames=100, interval=50, blit=True)

# Show the plot
plt.show()
